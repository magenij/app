﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WtfTest.Models;

namespace WtfTest
{
    /// <summary>
    /// Логика взаимодействия для WindowGroup.xaml
    /// </summary>
    public partial class WindowGroup : Window
    {
        //В этой части кода много тормозил и гуглил, решение оказалось очень простым
        public WindowGroup(IEnumerable<IGrouping<DateTime, Pallet>> data)
        {
            InitializeComponent();

            foreach (var date in data)
            {
                TreeViewItem item = new TreeViewItem();
                item.Header = date.Key;
                
                foreach (var e in date.OrderByDescending(d => d.Weight)) // Заменой тут и в нижней строке можно остортировать по любому параметру при необходимости
                {
                    TreeViewItem currentPallets = new TreeViewItem();
                    currentPallets.Header = $"{e.id}\t{e.ShelfLife:d}";
                    currentPallets.Tag = e;
                    item.Items.Add(currentPallets);
                }
                
                tv.Items.Add(item);
                
            }

        }

        private void Tv_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show($"Вес {((tv.SelectedItem as TreeViewItem).Tag as Pallet).Weight}"); // Коммент выше об этой строке
        }
    }
}
