﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using WtfTest.Models;
using Newtonsoft.Json;

namespace WtfTest.Infrastructure
{
    public class Repository
    {
        public Repository()
        {

        }

        // Если есть файл берём из него, если его нет генерим новые
        public ObservableCollection<Pallet> Pallets { get; set; }
        public Repository(string PathDb, int Count)
        {
            if (File.Exists(PathDb))
            {
                Pallets = JsonConvert.DeserializeObject<ObservableCollection<Pallet>>(File.ReadAllText(PathDb));
            }
            else
            {
                Pallets = new ObservableCollection<Pallet>();
                for (int i = 0; i < Count; i++)
                {
                    Pallets.Add(palletsStorage[i]);
                }
                File.WriteAllText(PathDb, JsonConvert.SerializeObject(Pallets));
            }
        }

        // Здесь только сортировка паллет по дате и группировка их, дальнейшая сортировка уже в классе с UI WindowGroup.xaml.cs
        public IEnumerable<IGrouping<DateTime, Pallet>> GroupByDate => Pallets.OrderBy(e => e.ShelfLife).GroupBy(e => e.ShelfLife); 
       

        static Repository()
        {
            r = new Random();
            boxStorage = new List<Box>();

            for (int i = 0; i < 100; i++)
            {
                boxStorage.Add(
                    new Box()
                    {
                        id = i,
                        height = RandomNumber(),
                        width = RandomNumber(),
                        depth= RandomNumber(),
                        DateOfManufacture = new DateTime(2019, RandomNumber(1,5), RandomNumber(1,5))
                    });
            }

            palletsStorage = new List<Pallet>();

            for (int i = 0; i < 200; i++)
            {
                palletsStorage.Add(
                    new Pallet()
                    {
                        id = i,
                        height = RandomNumber(),
                        width = RandomNumber(),
                        depth = RandomNumber(),
                        Boxes = new List<Box>
                        {
                            boxStorage[RandomNumber(max:boxStorage.Count)],
                            boxStorage[RandomNumber(max:boxStorage.Count)],
                            boxStorage[RandomNumber(max:boxStorage.Count)],
                            boxStorage[RandomNumber(max:boxStorage.Count)],
                            boxStorage[RandomNumber(max:boxStorage.Count)],
                        },

                    });
            }
 

        }

        

        static List<Box> boxStorage;
        static List<Pallet> palletsStorage;
        static Random r;
        static int RandomNumber(int min = 1, int max = 30) => r.Next(min, max);
    }
}
