﻿using System;

namespace WtfTest.Models
{
    public class Box : ObjectsInStock
    {
        int weight;
        public DateTime DateOfManufacture { get; set; }
        public DateTime ShelfLife { get { return DateOfManufacture + TimeSpan.FromDays(100); } }

        public override int Volume => base.width * base.height * base.depth;

        public override int Weight { get => this.weight; set => weight = value; }
    }
}
