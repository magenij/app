﻿namespace WtfTest.Models
{
    public abstract class ObjectsInStock
    {
        // Id, ширина, высота, глубина, вес
        // Решил в базовом оставить только, то что указано в задании

        public int id { get; set; }
        public  int width { get; set; }
        public int height { get; set; }
        public int depth { get; set; }
        public virtual int Weight { get; set; }
        public abstract int Volume { get; }
    }
}
