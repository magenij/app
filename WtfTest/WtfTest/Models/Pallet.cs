﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WtfTest.Models
{
    public class Pallet : ObjectsInStock
    {
        public List<Box> Boxes { get; set; }

        //берем минимальную дату для палеты
        public DateTime ShelfLife
        {
            get
            {
                return Boxes.Min(e => e.ShelfLife);
            }
        }

        //В двух методах считаем вес и объем
        public override int Weight
        {
            get
            {
                int sum = 30;
                foreach (var e in Boxes) { sum += e.width; }
                return sum;
            }

        }

        public override int Volume
        {
            get
            {
                int v = base.width + base.height + base.depth;
                foreach (var e in Boxes) { v += e.Volume; }
                return v;
            }
        }
      
    }
}
